package cepBr.validate;

import cepBr.model.Cep;

public class Validate {

	Cep responseValidate;
	public boolean cepValidate(String cep){
		if(!cep.matches("\\b\\d{8}\\b")){
			return false;
		}
		return true;
	}
	
	public void validateParams(String uf, String localidade, String logradouro) {
		if (uf == null || uf.isEmpty() || uf.length() != 2){
			responseValidate.setStatus("UF inválida - deve conter 2 caracteres: " + uf);
		}
		if (localidade == null || localidade.isEmpty() || localidade.length() < 3){
			responseValidate.setStatus("Localidade inválida - deve conter pelo menos 3 caracteres: " + localidade);
		}
		if (uf == null  || uf.isEmpty() || logradouro.length() < 3){
			responseValidate.setStatus("Logradouro inválido - deve conter pelo menos 3 caracteres: " + logradouro);
		}
	}
}
