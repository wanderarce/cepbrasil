package cepBr.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.jr.ob.JSONObjectException;

import cepBr.model.Cep;
import cepBr.validate.Validate;

public class CepService {

	private Cep cep;
	private List<Cep> ceps;
	
	private static final String SERVICE_HOST = "http://viacep.com.br/ws/";
	private static final String SERVICE_FORMAT = "/json/";

	protected JSON service;

	public CepService() {
		service = JSON.std;
	}

	public CepService(JSON service) {
		this.service = service;
	}

	public Cep getEndereco(String param) throws IOException {
		char[] chars = param.toCharArray();

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < chars.length; i++) {
			if (Character.isDigit(chars[i])) {
				builder.append(chars[i]);
			}
		}
		param = builder.toString();

		if (param.length() != 8) {
			throw new IllegalArgumentException("CEP inválido - deve conter 8 dígitos: " + cep);
		}

		String urlString = SERVICE_HOST + param + SERVICE_FORMAT;
		return openConnect(urlString);
	}

	private Cep openConnect(String urlString) throws MalformedURLException, IOException, JSONObjectException {
		URL url = new URL(urlString);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		try {
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			cep = getService().beanFrom(Cep.class, in);
			if (cep == null || cep.getCep() == null) {
				return null;
			}
			return cep;
		} finally {
			urlConnection.disconnect();
		}
	}

	public List<Cep> getEnderecos(String uf, String localidade, String logradouro) throws IOException {
		Validate validate = new Validate();

		validate.validateParams(uf, localidade, logradouro);

		String urlString = SERVICE_HOST+uf+"/"+localidade+"/"+logradouro+ SERVICE_FORMAT;
		URL url = new URL(urlString);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		try {
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			ceps = getService().listOfFrom(Cep.class, in);
			return ceps;
		} finally {
			urlConnection.disconnect();
		}

	}

	
	public JSON getService() {
		return service;
	}

	public void setService(JSON service) {
		this.service = service;
	}

}