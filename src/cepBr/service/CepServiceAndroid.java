package cepBr.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import cepBr.model.Cep;
import cepBr.validate.Validate;

public class CepServiceAndroid {

	private static final String SERVICE = "http://viacep.com.br/ws/";
	private Cep cepEncontrado;
	private static List<Cep> listCeps = new ArrayList<>();
	private Validate isValid = new Validate();
	private StringBuilder result;
	private URL url;
	private HttpURLConnection conn;
	private BufferedReader bufferedReader;

	public String openConnect(String urlService) throws IOException {

		try {
			result = new StringBuilder();
			url = new URL(urlService);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");

			bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line;

			if (bufferedReader != null) {
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
			}

		} catch (MalformedURLException | ProtocolException e) {
			e.getMessage();
			e.getCause();
		} catch (IOException e) {
			e.getMessage();
			e.getCause();
		} finally {
			conn.disconnect();
		}

		return result.toString();
	}

	/**
	 * O parametro cep deve conter exatamente 8 carateres coforme especicacao do
	 * proprio site que disponibiliza o servico
	 * 
	 * @param cep
	 * @throws Exception
	 */
	public Cep requestCep(String cep) throws Exception {
		cepEncontrado = new Cep();
		if (!isValid.cepValidate(cep)) {
			cepEncontrado.setStatus("Cep informado inválido!");
		}
		cepEncontrado = responseCep(cep);
		
		return cepEncontrado;
	}

	private Cep responseCep(String cep) throws JSONException, IOException {
		String url = SERVICE + cep + "/json/";
		JSONObject obj = new JSONObject(openConnect(url));

		cepEncontrado = new Cep();
		if (!obj.has("erro")) {
			cepEncontrado.setCep(obj.getString("cep"));
			cepEncontrado.setLogradouro(obj.getString("logradouro"));
			cepEncontrado.setComplemento(obj.getString("complemento"));
			cepEncontrado.setBairro(obj.getString("bairro"));
			cepEncontrado.setLocalidade(obj.getString("localidade"));
			cepEncontrado.setUf(obj.getString("uf"));
			cepEncontrado.setIbge(obj.getString("ibge"));
			cepEncontrado.setGia(obj.getString("gia"));
			cepEncontrado.setStatus("success");
		}
		if (cepEncontrado.getCep() == null || obj.has("erro") || obj == null) {
			cepEncontrado.setStatus("Cep informado não encontrado");
		}
		return cepEncontrado;

	}

	/**
	 * requisicao como os 3 parametros abaixo e obrigatorio conforme descricao
	 * de servico do proprio site
	 * 
	 * @param uf
	 * @param localidade
	 * @param logradouro
	 * @throws JSONException
	 * @throws IOException
	 */
	public List<Cep> requestCeps(String uf, String localidade, String logradouro) throws JSONException, IOException {
		Validate validate = new Validate();
		validate.validateParams(uf, localidade, logradouro);

		String url = SERVICE + uf + "/" + localidade + "/" + logradouro + "/json/";

		JSONArray ceps = new JSONArray(openConnect(url));

		return responseCeps(ceps);
	}

	private List<Cep> responseCeps(JSONArray ceps) throws JSONException {
		listCeps.clear();
		if (ceps.length() > 0) {

			
			for (int i = 0; i < ceps.length(); i++) {
				JSONObject obj = ceps.getJSONObject(i);
				if (!obj.has("erro") || !obj.equals("[")) {
					cepEncontrado = new Cep();
					cepEncontrado.setCep(obj.getString("cep"));
					cepEncontrado.setLogradouro(obj.getString("logradouro"));
					cepEncontrado.setComplemento(obj.getString("complemento"));
					cepEncontrado.setBairro(obj.getString("bairro"));
					cepEncontrado.setLocalidade(obj.getString("localidade"));
					cepEncontrado.setUf(obj.getString("uf"));
					cepEncontrado.setIbge(obj.getString("ibge"));
					cepEncontrado.setGia(obj.getString("gia"));
					cepEncontrado.setStatus("success");
					// insere o novo CEP
					listCeps.add(cepEncontrado);
				}
			}
		}
		if (ceps.length() < 1) {
			cepEncontrado = new Cep();
			cepEncontrado.setStatus("Nenhum resultado encontrado com os parâmetros informados!");
			listCeps.add(cepEncontrado);
		}
		return listCeps;

	}

}
